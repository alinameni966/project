<?php return array(
    'root' => array(
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'type' => 'project',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => NULL,
        'name' => 'ali/composer-test',
        'dev' => true,
    ),
    'versions' => array(
        'ali/composer-test' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'type' => 'project',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
        'ybazli/faker' => array(
            'pretty_version' => 'v1.0.1',
            'version' => '1.0.1.0',
            'type' => 'laravel',
            'install_path' => __DIR__ . '/../ybazli/faker',
            'aliases' => array(),
            'reference' => '8ed1b8a5a658e2bb028f62eb6186e534a7972448',
            'dev_requirement' => false,
        ),
    ),
);
