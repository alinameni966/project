<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit9d7ca0826b61ee26e4a59df7824fc046
{
    public static $prefixLengthsPsr4 = array (
        'Y' => 
        array (
            'Ybazli\\Faker\\' => 13,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Ybazli\\Faker\\' => 
        array (
            0 => __DIR__ . '/..' . '/ybazli/faker/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit9d7ca0826b61ee26e4a59df7824fc046::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit9d7ca0826b61ee26e4a59df7824fc046::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit9d7ca0826b61ee26e4a59df7824fc046::$classMap;

        }, null, ClassLoader::class);
    }
}
